const got = require('got');
const SERVICE_URL = process.env.SERVICE_URL || "http://127.0.0.1:5000/";

console.log(`Server URL to be used: '${SERVICE_URL}'`);

describe("Test API testing", () => {
	describe ("Retrieving some info", () => {
		const options = {
			method: "GET",
			headers: { 'Content-type': 'application/json' }
		};

		it("Retrieved all events", async () => {
			const events = await got(SERVICE_URL + "event", options).json();

			expect(events.length).toBe(3);
		});
	});

	describe ("Changing info", () => {
		const options = {
			method: "PUT",
			headers: { 'Content-type': 'application/json' },
			json: {
				title: "New title"
			}
		};

		it("Retrieved all events", async () => {
			const newEvent = await got(SERVICE_URL + "event/2", options).json();

			expect(newEvent.title).toEqual('New title');
		});
	});
});