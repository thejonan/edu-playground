/**
 * A unit tests placeholder
 * 
 * Copyright (C) 2021, Ivan (Jonan) Gueorguiev
 */
const Data = require('../specs/test-data.json');
const Event = require('../src/Event')(Data);
const Participant = require('../src/Participant')(Data);

describe("Tests set descriptions", () => {
	// beforeAll(async () => { console.log("Do something async here!") });

	const evData = {
		"title": "Season test",
		"organizer": "BORA Sailing",
		"description": "This is the event in the very end of the season",
		"duration": 3,
		"starting": "2021-06-01",
		"participants": []
	};

	describe("Nested tests - simple addition", () => {
		const newEvent = Event.addNewEvent(evData);
		
		
		it("Has proper number of participants", () => {
			console.log(newEvent);
			expect(newEvent.participants.length).toBe(0);
		});

		it("Has expected static fields", () => {
			expect(newEvent.title).toEqual(evData.title);
			expect(newEvent.organizer).toEqual(evData.organizer);
		});
	});

	describe("Nested tests - adding some participants", () => {
		const newEvent = Event.addNewEvent(evData);

		Participant.registerParticipant(newEvent.id, {
			"name": "Yasen Kachinski",
			"age": 42,
			"registered": "2020-12-01" 
		});
		
		it("Has proper number of participants", () => {
			expect(newEvent.participants.length).toBe(1);
		});

		it("Has expected static fields", () => {
			expect(newEvent.title).toEqual(evData.title);
			expect(newEvent.organizer).toEqual(evData.organizer);
		});

	});

});
