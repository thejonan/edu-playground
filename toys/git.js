const assert = require('assert').strict;

/**
 * Some stupid test function
 * @param {Array} arr 
 */
function funcOne(arr) {
	// TODO: This is/was line #8
	assert.ok(Array.isArray(arr));

	const obj = {};
	for (let i = 0;i < arr.length; ++i) {
		const entry = arr[i];
		obj[entry[0]] = entry[1];
	}

	return obj;
	// TODO: This is/was line #18 on 1-task-one
};

/**
 * Another stupid function
 * @param {Object} obj 
 * @param {String} str 
 */
function funcTwo(obj, str) {
	// TODO: This was/is line #18
	assert.ok(obj);
	const arr = [];
	for (const key in obj)
		if (key.indexOf(str) > -1)
			arr.push(obj[key]);
	
	return arr;
	// TODO: This is/was line #26 on 2-task-two
}

// TODO: Execution happens on line #23:
console.log(funcTwo(funcOne([['foo', 'bar'], ['мяу', 'бау']])));
