const assert = require('assert').strict;

function fakeWorker(period, winding, handler) {
	const schedule = () => setTimeout(() => {
		if (handler() !== false)
			schedule();
	}, period + Math.random() * 2. * winding - winding);

	schedule();
}

const slowSummer = (a, b, cb) => setTimeout(() => cb(a + b), (a + b) * 10);
const slowMultiplier = (a, b, cb) => setTimeout(() => cb(a * b), (a + b) * 10);

const asyncSummer = async (a, b) => a + b;
const asyncMultiplier = async (a, b) => a * b;

const promisifyVal = (val) => new Promise((res, rej) => {
	if (val > 0)
		res(val);
	else
		rej(`Error: nonpositive passed ${val}!`);
});

function Demo_Parallelism() {
	let cnt = 0;

	fakeWorker(400, 50, () => {
		console.log(`1: counter ${cnt}`);
		return cnt++ < 20;
	});

	fakeWorker(600, 200, () => {
		console.log(`2: counter ${cnt}`);
		return cnt++ < 20;
	});
}

function Demo_DeepNesting(a, b, c, d) {
	slowMultiplier(a, b, (amb) => {
		slowMultiplier(c, d, (cmd) => {
			slowSummer(amb, cmd, (res) => {
				console.log(`Slow result: ${res}`);
			});
		});
	});
}

function Demo_Thening(a, b, c, d) {	
	asyncMultiplier(a, b).then((amb) => {
		asyncMultiplier(c, d).then((cmd) => {
			asyncSummer(amb, cmd).then((res) => {
				console.log(`Thenable result: ${res}`);
			});
		});
	})
	.catch((err) => console.error(err));
}

async function Demo_Asyncing(a, b, c, d) {
	const amb = asyncMultiplier(a, b),
		cmd = asyncMultiplier(c, d),
		res = asyncSummer(amb, cmd);
	console.log(`Async result: ${res}`);;
}

async function Demo_Promisified(val) {
	try {
		console.log(`Promisified: ${await promisifyVal(val)}`);
	} catch (err) {
		console.error(err);
	}
}

Demo_Parallelism();
// Demo_DeepNesting(4, 5, 8, 9);
// Demo_Thening(4, 5, 8, 9);
// Demo_Asyncing(4, 5, 8, 9);
// Demo_Promisified(1);
// Demo_Promisified(-1);
