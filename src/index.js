/**
 * The public API endpoint wiring based on the OpenAPI file
 * 
 * Copyright (C) 2021, InsInCloud LLC
 */
const express = require('express');
require('express-async-errors');
const _ = require('lodash');

const apiInitialize = require('express-openapi').initialize;
const swaggerUi = require('swagger-ui-express');
const apiHandlers = require('./Handlers');

const apiSpec = require('./openapi.json');
const docsPath = process.env.PORT || 'docs';

const errorTransformer = (apiErr) => {
	// TODO: Use an error template here!
	console.error(`ERR: ${apiErr.message}`);
	return { message: `[${apiErr.errorCode}] @ '${apiErr.location}': ${apiErr.message}` };
};

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

apiInitialize({
	app,
	apiDoc: apiSpec,
	docsPath: docsPath,
	operations: apiHandlers,
	errorTransformer
});

app.use(docsPath, swaggerUi.serve, swaggerUi.setup(apiSpec))

app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({ error: { message: error.message } });
	next();
});

const port = process.env.PORT || 5000;
console.log(`Listening on port: ${port}`);
app.listen(port);
