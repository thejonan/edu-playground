/**
 * Participants endpoints
 * Copyright (C) 2021, Ivan (Jonan) Gueorguiev
 */


module.exports = function (events) {
	return {
		getParticipants: function (eventId) {
			const currentParticipant = events.find(x => x.id == eventId);
			if(currentParticipant.participants) {
				return currentParticipant.participants;
			} else {
				return null;
			}
		},

		registerParticipant: (eventId, participant) => {
			const event = events.find(x => x.id == eventId);
			event.participants.push(participant);
			
			return event;
		}
	};
};
