/**
 * Events endpoints
 * Copyright (C) 2021, Ivan (Jonan) Gueorguiev
 */
const _ = require('lodash');

module.exports = function (data) {
	return {
		addNewEvent: function (event) {
			const _event = { ...event };
			_event.id = data.length + 1;
			if (!_event.paricipants) _event.participants = [];
			data.push(_event);

			return _event;
		},
		getEvent: function (eventId) {
			return data.find(x => x.id == eventId);

			// throw new Error(`Not implemented yet!`);
		},
		updateEvent: function (id, event) {
			const ev = data.find(x => x.id == id);
			Object.keys(event)
			.forEach(key => {
				ev[key] = event[key]
			});
			
			return ev;
		},
		getAllEvents: function () {
			return _.map(data, (e) => _.pick(e, ['title', 'organizer', 'description', 'duration', 'starting']));
		}
	}
};
