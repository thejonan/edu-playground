/**
 * Events endpoints
 * Copyright (C) 2021, Ivan (Jonan) Gueorguiev
 */

// A simple hack to avoid the need for any databases
const Data = require('../specs/test-data.json');

const Event = require('./Event')(Data);
const Participant = require('./Participant')(Data);

const addNewEvent = async (req, res) => {
	res.status(200).send(Event.addNewEvent(req.body)).end();
};

const getAllEvents = async (req, res) => {
	res.status(200).send(Event.getAllEvents()).end();
};

const updateEvent = async (req, res) => {
	res.status(200).send(Event.updateEvent(req.params.eventId, req.body)).end();
};

const getEvent = async (req, res) => {
	res.status(200).send(Event.getEvent(req.params.eventId)).end();
};

const registerParticipant = async (req, res) => {
	const { eventId } = req.params;
	const participant = req.body;
	const updatedEvent = Participant.registerParticipant(eventId, participant) 

	return res.status(200).send(updatedEvent);
};

const getParticipants = async (req, res) => {
	res.status(200).send(Participant.getParticipants(req.params.eventId)).end();
};

module.exports = {
	addNewEvent,
	getAllEvents,
	updateEvent,
	getEvent,
	registerParticipant,
	getParticipants
};
